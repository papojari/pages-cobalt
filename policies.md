---
title: Policies
layout: default.liquid
categories: [ meta ]
tags: [ policies meta ]
---
- This website uses some of the rules from [this manifesto](https://jeffhuang.com/designed_to_last/). These are:
  - > Return to vanilla HTML/CSS
  - > Don't minimize that HTML
  - > Obsessively compress your images
- Minimal JavaScript use on this website, except for the find-billy game.
- No compromising the privacy of visitors by tracking them or similar.

If you think that this website doesn't adhere to it's policies, please file an issue [here](https://codeberg.org/papojari/pages-cobalt/issues).